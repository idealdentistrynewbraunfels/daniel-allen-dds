Family dentist providing a full range of dental services in a modern office with a focus on pain free dentistry. Daniel Allen,DDS in New Braunfels runs a family owned business that caters to perfecting everyone's smile. Dentures, teeth extractions, dental implants, and cosmetic porcelain restoration.

Address: 1099 N. Walnut Ave, Suite B, New Braunfels, TX 78130

Phone: 830-625-2222